package edu.pk.wm.activities

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager.PROVIDERS_CHANGED_ACTION
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import edu.pk.wm.R
import edu.pk.wm.handlers.CommonUtil
import edu.pk.wm.handlers.LocationHandler
import edu.pk.wm.receivers.GpsSwitchStateReceiver
import edu.pk.wm.receivers.InternetStateReceiver
import edu.pk.wm.services.ComponentsAvailableService
import edu.pk.wm.services.LocationService

/**
 * Maps Activity uses Google Api.
 *
 */
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private val sharedPreferences = "shared_preferences"
    private val switchIsChecked = "switch_is_checked"

    private lateinit var googleMap: GoogleMap
    private lateinit var locationHandler: LocationHandler
    private lateinit var gpsSwitchStateReceiver: GpsSwitchStateReceiver
    private lateinit var internetStateReceiver: InternetStateReceiver

    private var isChecked: Boolean = false
    private var apiKey: String? = ""

    /**
     * The method run when creating activities, launches other methods necessary for proper operation.
     *
     * @param savedInstanceState is a reference to a Bundle object that is passed into the onCreate method of every Android activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        apiKey = intent.getStringExtra("api_key")

        val toolbar = findViewById<Toolbar>(R.id.maps_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        loadData()
        initMap()
    }

    /**
     * Initializes the map.
     *
     */
    private fun initMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    /**
     * When the map is ready, it shows the user's location and registers receivers.
     *
     * @param googleMap - google map object
     */
    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        this.googleMap.isMyLocationEnabled = true
        this.googleMap.uiSettings.isMyLocationButtonEnabled = false

        locationHandler = LocationHandler(googleMap, this)
        locationHandler.getDeviceLocation()

        gpsSwitchStateReceiver = GpsSwitchStateReceiver()
        registerReceiver(gpsSwitchStateReceiver, IntentFilter(PROVIDERS_CHANGED_ACTION))

        internetStateReceiver = InternetStateReceiver()
        registerReceiver(internetStateReceiver,  IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    /**
     * Reads and sets reacts to items from the toolbar.
     *
     * @param menu - maps_toolbar.xml
     * @return true
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.maps_toolbar, menu)

        val itemSwitch = menu?.findItem(R.id.on_off_service)
        itemSwitch?.setActionView(R.layout.use_switch)

        val onOffService = menu?.findItem(R.id.on_off_service)?.actionView?.findViewById<Switch>(
            R.id.action_switch
        )
        onOffService?.isChecked = isChecked
        onOffService?.setOnCheckedChangeListener { compoundButton, isChecked ->
            this.isChecked = compoundButton.isChecked
            saveData()

            if (compoundButton.isChecked) {
                changeSwitchToSelected()
            } else {
                changeSwitchToUnselected()
            }
        }
        return true
    }

    /**
     * Reaction to starting the application running in the background.
     *
     */
    private fun changeSwitchToSelected() {
        val locationService = Intent(this@MapsActivity, LocationService::class.java)
        locationService.putExtra("api_key", apiKey)
        startForegroundService(locationService)

        val componentsAvailableService = Intent(this@MapsActivity, ComponentsAvailableService::class.java)
        startForegroundService(componentsAvailableService)

        CommonUtil().showToast(this, R.string.background_work_enabled)
    }

    /**
     * Reaction to deactivating the application in the background.
     *
     */
    private fun changeSwitchToUnselected() {
        stopService(Intent(this@MapsActivity, LocationService::class.java))
        stopService(Intent(this@MapsActivity, ComponentsAvailableService::class.java))

        CommonUtil().showToast(this, R.string.background_work_disenabled)
    }

    /**
     * Reaction on click log out icon. Goes back to main activity.
     *
     * @param item - item from menu
     */
    fun logOut(item: MenuItem?) {
        this.isChecked = false
        saveData()
        stopService(Intent(this@MapsActivity, LocationService::class.java))

        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("is_logged", "false")
        startActivity(intent)
    }

    /**
     * Saves shared preferences - switch state - responsible for running the application in the background.
     *
     */
    private fun saveData() {
        val sharedPreferences = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(switchIsChecked, isChecked)
        editor.apply()
    }

    /**
     * Loads shared preferences.
     *
     */
    private fun loadData() {
        val sharedPreferences = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)
        isChecked = sharedPreferences.getBoolean(switchIsChecked, false)
    }

    /**
     * Reaction on termination of activity. Unregisters receivers.
     *
     */
    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(gpsSwitchStateReceiver)
        unregisterReceiver(internetStateReceiver)
    }

    /**
     * Reaction on back key. Skip the main activity, to go to it use the log out button.
     *
     * @param keyCode - key code
     * @param event - key event
     * @return calling the super method
     */
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val intent = Intent(this@MapsActivity, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra("exit", true)
            startActivity(intent)
        }
        return super.onKeyDown(keyCode, event)
    }
}
