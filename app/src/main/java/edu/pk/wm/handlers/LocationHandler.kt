package edu.pk.wm.handlers

import android.content.Context
import android.location.Location
import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import edu.pk.wm.R
import edu.pk.wm.components.ComponentsAvailable

/**
 * Supports map activity in terms of location
 *
 * @property googleMap - google map object
 * @property context - activity context
 */
class LocationHandler (private val googleMap: GoogleMap, private val context: Context) {

    private val defaultZoom = 15f

    private var locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            if (locationResult == null) {
                return
            }
            for (location in locationResult.locations) {
                if (location != null) {
                   moveCamera(LatLng(location.latitude, location.longitude))
                }
            }
        }
    }

    /**
     * Is responsible for returning the user's current position on the map. Reacts to location changes.
     *
     */
    fun getDeviceLocation() {
        val locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000L
        locationRequest.fastestInterval = 1000L

        try {
            val fusedLocationProviderClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            val lastLocation = fusedLocationProviderClient.lastLocation

            lastLocation.addOnSuccessListener { location: Location? ->
                if (location != null && ComponentsAvailable().verifyAvailableNetwork(context)) {
                    moveCamera(LatLng(location.latitude, location.longitude))
                } else {
                    if (!ComponentsAvailable().checkGpsAvailable(context) || !ComponentsAvailable().verifyAvailableNetwork(context)) {
                        CommonUtil().showToast(context, R.string.warning_when_obtaining_device_location)
                    }
                    fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null)
                }
            }
        } catch (e: SecurityException) {
            Log.e("edu.pk.wm.activities.MapsActivity.getDeviceLocation", e.printStackTrace().toString())
        }
    }

    /**
     * Approximates the given coordinates
     *
     * @param latLng - latitude and longitude
     */
    private fun moveCamera(latLng: LatLng) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, defaultZoom))
    }
}