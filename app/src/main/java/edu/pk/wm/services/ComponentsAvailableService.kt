package edu.pk.wm.services

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import edu.pk.wm.R
import edu.pk.wm.receivers.GpsNotificationReceiver
import edu.pk.wm.receivers.InternetNotificationReceiver

/**
 * The service starts with the activation of the background action, informs about a sudden lack of access to gps or the internet.
 *
 */
class ComponentsAvailableService: Service() {

    private lateinit var gpsNotificationReceiver: GpsNotificationReceiver
    private lateinit var internetNotificationReceiver: InternetNotificationReceiver

    /**
     * Registers receivers checking component availability
     *
     * @param intent - the Intent supplied to Context.startService(Intent)
     * @param flags - additional data about this start request
     * @param startId - a unique integer representing this specific request to start
     * @return
     */
    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        gpsNotificationReceiver = GpsNotificationReceiver()
        registerReceiver(gpsNotificationReceiver, IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION))

        internetNotificationReceiver = InternetNotificationReceiver()
        registerReceiver(internetNotificationReceiver,  IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

        val notification = createNotification()
        startForeground(1, notification)

        return START_NOT_STICKY
    }

    /**
     * Creates notification
     *
     * @return NotificationCompat.Builder
     */
    private fun createNotification(): Notification? {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel()
            } else {
                ""
            }

        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        return notificationBuilder.setOngoing(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setPriority(NotificationCompat.PRIORITY_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
    }

    /**
     * Creates notification channel - Android 8.0 required
     *
     * @return channel id
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String{
        val channelId = "components_available_channel"
        val channel = NotificationChannel(channelId, "Components Available Channel", NotificationManager.IMPORTANCE_NONE)
        channel.lightColor = Color.BLUE
        channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(channel)

        return channelId
    }

    /**
     * Unregisters receivers, stops service
     *
     */
    override fun onDestroy() {
        super.onDestroy()

        unregisterReceiver(gpsNotificationReceiver)
        unregisterReceiver(internetNotificationReceiver)

        stopForeground(true)
        stopSelf()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}