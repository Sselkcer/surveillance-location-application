package edu.pk.wm.receivers

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.LocationManager.PROVIDERS_CHANGED_ACTION
import androidx.core.app.NotificationCompat
import edu.pk.wm.handlers.Channel.Companion.channelId
import edu.pk.wm.R
import edu.pk.wm.components.ComponentsAvailable

/**
 * Receiver for external notifications. Notifications related to gps.
 *
 */
class GpsNotificationReceiver: BroadcastReceiver() {

    /**
     * If the action concerns gps on / off.
     *
     * @param context - activity context
     * @param intent - intent with action
     */
    override fun onReceive(context: Context, intent: Intent) {
        if (PROVIDERS_CHANGED_ACTION == intent.action) {
            Thread.sleep(1000)
            if (!ComponentsAvailable().checkGpsAvailable(context)) {
                sendNotification(context)
            }
        }
    }

    /**
     * Sends warning in the form of an external notification that gps has been disabled
     *
     * @param context - activity context
     */
    private fun sendNotification(context: Context) {
        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.warning)
            .setContentTitle(context.resources.getString(R.string.warning))
            .setContentText(context.resources.getString(R.string.gps_notification))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)

        val notificationManager: NotificationManager? = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

        notificationManager?.notify(1111, notificationBuilder.build())
    }
}