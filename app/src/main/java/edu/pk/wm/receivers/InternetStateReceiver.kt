package edu.pk.wm.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import edu.pk.wm.R
import edu.pk.wm.components.ComponentsAvailable
import edu.pk.wm.handlers.CommonUtil

/**
 * Receiver responding to a change in the status of the internet(Wi-Fi) button - internal notification
 *
 */
class InternetStateReceiver: BroadcastReceiver()  {

    /**
     * Sends warning in the form of an internal notification that internet(Wi-Fi) has been disabled
     *
     * @param context - activity context
     */
    override fun onReceive(context: Context, intent: Intent) {
        if (ConnectivityManager.CONNECTIVITY_ACTION == intent.action) {
            Thread.sleep(1000)
            if (!ComponentsAvailable().verifyAvailableNetwork(context)) {
                CommonUtil().showToast(context, R.string.internet_toast)
            }
        }
    }
}