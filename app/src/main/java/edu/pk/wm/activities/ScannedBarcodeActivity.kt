package edu.pk.wm.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.Detector.Detections
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import edu.pk.wm.R
import java.io.IOException

/**
 * Activity for scanning qr code
 *
 */
class ScannedBarcodeActivity : AppCompatActivity() {

    private val requestCameraPermission = 201

    private var barcodeDetector: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    private var confirmButton: Button? = null
    private var surfaceView: SurfaceView? = null
    private var barcodeValue: TextView? = null
    private var intentData = ""

    /**
     * The method run when creating activities, sets the listener to a button.
     *
     * @param savedInstanceState is a reference to a Bundle object that is passed into the onCreate method of every Android activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanned_barcode)
        supportActionBar?.title = Html.fromHtml("<font color='#003300'>&nbsp;&nbsp;&nbsp;Surveillance Location Application</font>")


        barcodeValue = findViewById(R.id.barcodeValue)
        surfaceView = findViewById(R.id.surfaceView)
        confirmButton = findViewById(R.id.confirm)
        confirmButton?.setOnClickListener(View.OnClickListener {
            if (intentData.isNotEmpty()) {
                val intent = Intent(this@ScannedBarcodeActivity, MainActivity::class.java)
                intent.putExtra("api_key", intentData)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        })
    }

    /**
     * Calls the release method from CameraSource
     *
     */
    override fun onPause() {
        super.onPause()
        cameraSource!!.release()
    }

    /**
     * Initialise detectors and sources - on resume
     *
     */
    override fun onResume() {
        super.onResume()
        initialiseDetectorsAndSources()
    }

    /**
     * Initialise detectors and sources - asks to use the camera, assigns appropriate permits. Validates the detected code.
     *
     */
    private fun initialiseDetectorsAndSources() {
        barcodeDetector = BarcodeDetector.Builder(this)
            .setBarcodeFormats(Barcode.ALL_FORMATS)
            .build()
        cameraSource = CameraSource.Builder(this, barcodeDetector)
            .setRequestedPreviewSize(1920, 1080)
            .setAutoFocusEnabled(true)
            .build()

        surfaceView!!.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                try {
                    if (ActivityCompat.checkSelfPermission(this@ScannedBarcodeActivity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource?.start(surfaceView!!.holder)
                    } else {
                        ActivityCompat.requestPermissions(this@ScannedBarcodeActivity, arrayOf(Manifest.permission.CAMERA), requestCameraPermission)
                    }
                } catch (e: IOException) {
                    Log.e("edu.pk.wm.activities.ScannedBarcodeActivity.initialiseDetectorsAndSources", e.printStackTrace().toString())
                }
            }

            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {}

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                cameraSource?.stop()
            }
        })

        barcodeDetector?.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {}

            override fun receiveDetections(detections: Detections<Barcode>) {
                val barCodes = detections.detectedItems
                if (barCodes.size() != 0) {
                    barcodeValue!!.post {
                        intentData = barCodes.valueAt(0).displayValue
                        barcodeValue!!.text = intentData
                    }
                }
            }
        })
    }

    /**
     * Responds to the user's decision
     *
     * @param requestCode - request code
     * @param permissions - permissions
     * @param grantResults - array of results
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode) {
            requestCameraPermission -> {
                if (grantResults.isNotEmpty()) {
                    recreate()
                }
            }
        }
    }
}
