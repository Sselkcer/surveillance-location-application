package edu.pk.wm.services

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MIN
import edu.pk.wm.R
import edu.pk.wm.components.ComponentsAvailable
import edu.pk.wm.requests.SaveLocationRequest
import org.json.JSONException
import org.json.JSONObject

/**
 * The service starts with the activation of the background action, constantly monitors the user's location in the background
 *
 */
class LocationService : Service() {

    private var locationManager: LocationManager? = null
    private var apiKey: String? = ""
    private var locationListener  = object : LocationListener {
        /**
         * Sends latitude and longitude to the database
         *
         * @param location - location object
         */
        override fun onLocationChanged(location: Location) {
            if (ComponentsAvailable().verifyAvailableNetwork(applicationContext)) {
            val postData = JSONObject()
            try {
                postData.put("lat", location.latitude)
                postData.put("long", location.longitude)
                SaveLocationRequest()
                    .execute("https://fast-badlands-93869.herokuapp.com/api/mobile/location", postData.toString(), apiKey)
            } catch (e: JSONException) {
                Log.e("edu.pk.wm.LocationService.onLocationChanged", e.toString())
            }
                Log.i("edu.pk.wm.services.LocationService.onLocationChanged", "${location.latitude} ${location.longitude}")
            }
        }

        override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {}

        override fun onProviderEnabled(s: String) {}

        override fun onProviderDisabled(s: String) {}
    }

    /**
     * Initiates operation of the service
     *
     * @param intent - the Intent supplied to Context.startService(Intent)
     * @param flags - additional data about this start request
     * @param startId - a unique integer representing this specific request to start
     * @return
     */
    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        apiKey = intent.getStringExtra("api_key")

        val notification = createNotification()
        startForeground(1, notification)
        locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager!!.requestLocationUpdates(GPS_PROVIDER, 3000, 5f, locationListener)

        return START_NOT_STICKY
    }

    /**
     * Creates notification
     *
     * @return NotificationCompat.Builder
     */
    private fun createNotification(): Notification? {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel()
            } else {
                ""
            }

        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        return notificationBuilder.setOngoing(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setPriority(PRIORITY_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
    }

    /**
     * Creates notification channel - Android 8.0 required
     *
     * @return channel id
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String{
        val channelId = "location_service_channel"
        val channel = NotificationChannel(channelId, "Location Service Channel", NotificationManager.IMPORTANCE_NONE)
        channel.lightColor = Color.BLUE
        channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(channel)

        return channelId
    }

    /**
     * Unregisters receivers, stops service
     *
     */
    override fun onDestroy() {
        super.onDestroy()

        stopForeground(true)
        stopSelf()
        locationManager!!.removeUpdates(locationListener)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }
}