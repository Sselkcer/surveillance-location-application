package edu.pk.wm.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.LocationManager.PROVIDERS_CHANGED_ACTION
import edu.pk.wm.R
import edu.pk.wm.components.ComponentsAvailable
import edu.pk.wm.handlers.CommonUtil

/**
 * Receiver responding to a change in the status of the gps button - internal notification
 *
 */
class GpsSwitchStateReceiver: BroadcastReceiver() {

    /**
     * Sends warning in the form of an internal notification that gps has been disabled
     *
     * @param context - activity context
     */
    override fun onReceive(context: Context, intent: Intent) {
        if (PROVIDERS_CHANGED_ACTION == intent.action) {
            Thread.sleep(1000)
            if (!ComponentsAvailable().checkGpsAvailable(context)) {
                CommonUtil().showToast(context, R.string.gps_toast)
            }
        }
    }
}