package edu.pk.wm.handlers

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import edu.pk.wm.R

/**
 * Decorator for the Toast object
 *
 */
class CommonUtil {

    /**
     * Displays a custom look for Toast notifications
     *
     * @param context - activity context
     * @param stringId - id from res/values/strings.xml
     */
    fun showToast(context: Context, stringId: Int) {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast, (context as Activity).findViewById(R.id.toast_layout))
        (layout.findViewById<View>(R.id.toast_text) as TextView).text = context.getResources().getString(stringId)

        val toast = Toast(context)
        toast.duration = Toast.LENGTH_LONG
        toast.view = layout
        toast.show()
    }
}