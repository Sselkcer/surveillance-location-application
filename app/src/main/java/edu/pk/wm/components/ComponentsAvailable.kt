package edu.pk.wm.components

import android.content.Context
import android.location.LocationManager
import android.net.ConnectivityManager

/**
 * A class that checks the availability of necessary components - internet and gps
 *
 */
class ComponentsAvailable {

    /**
     * Checks if GPS is enabled
     *
     * @param context - activity context
     * @return true if is enabled
     */
    fun checkGpsAvailable(context: Context): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    /**
     * Checks if internet (or Wi-Fi) is enabled
     *
     * @param context - activity context
     * @return true if is enabled
     */
    fun verifyAvailableNetwork(context: Context): Boolean {
        val connectivityManager= context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo= connectivityManager.activeNetworkInfo

        return  networkInfo != null && networkInfo.isConnected
    }
}