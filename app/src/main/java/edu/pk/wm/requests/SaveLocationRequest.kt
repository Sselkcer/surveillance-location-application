package edu.pk.wm.requests

import android.os.AsyncTask
import android.util.Log
import java.io.DataOutputStream
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

/**
 * Deals with asynchronous execution of the POST request, sends current location
 *
 */
class SaveLocationRequest : AsyncTask<String?, Void?, String>() {

    /**
     * Executes a background query - asynchronously
     *
     * @param params - [0] - url, [1] - body, [2] - token
     * @return response body
     */
    override fun doInBackground(vararg params: String?): String {
        val responseBody = StringBuilder()
        var connection: HttpURLConnection? = null
        try {
            val url = params[0]!!
            val body = params[1]!!
            val token = params[2]!!

            connection = URL(url).openConnection() as HttpURLConnection
            connection.requestMethod = "POST"
            connection.doOutput = true
            connection.setRequestProperty("Authorization", "Bearer $token")
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8")

            val writer = DataOutputStream(connection.outputStream)
            writer.writeBytes(body)
            writer.flush()
            writer.close()

            val inputStream: InputStream = connection.inputStream
            val inputStreamReader = InputStreamReader(inputStream)
            var inputStreamData: Int = inputStreamReader.read()
            while (inputStreamData != -1) {
                val current = inputStreamData.toChar()
                inputStreamData = inputStreamReader.read()
                responseBody.append(current)
            }
        } catch (e: Exception) {
            Log.e("edu.pk.wm.AsyncPostRequest.doInBackground", e.toString())
        } finally {
            connection?.disconnect()
        }
        return responseBody.toString()
    }
}