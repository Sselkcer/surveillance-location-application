package edu.pk.wm.activities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Html
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import edu.pk.wm.R
import edu.pk.wm.handlers.CommonUtil
import edu.pk.wm.requests.CurrentLoggedDeviceRequest
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.lang.Exception

/**
 * Main Activity. Allows you to enter or scan a qr code, then go to Maps Activity.
 *
 */
class MainActivity : AppCompatActivity() {

    private val fineLocation = Manifest.permission.ACCESS_FINE_LOCATION
    private val coarseLocation = Manifest.permission.ACCESS_COARSE_LOCATION
    private val locationPermissionRequestCode = 1234
    private val apiKeyIsPresent = 1236
    private val sharedPreferences = "shared_preferences"
    private val userIsLogged = "user_is_logged"
    private val flag = false

    private var isLogged: Boolean = false

    /**
     * The method run when creating activities, launches other methods necessary for proper operation.
     * If the user is logged in, skips this activity.
     *
     * @param savedInstanceState is a reference to a Bundle object that is passed into the onCreate method of every Android activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.title = Html.fromHtml("<font color='#003300'>&nbsp;&nbsp;&nbsp;Surveillance Location Application</font>")

        getLocationPermission()
        setListeners()

        if (loadExtras()) return

        loadData()
        if (isLogged) {
            startActivity(Intent(this@MainActivity, MapsActivity::class.java))
        }
    }

    /**
     * Checks if permissions are granted: fineLocation and coarseLocation. Otherwise application requests for it.
     *
     */
    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.applicationContext, fineLocation) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(this.applicationContext, coarseLocation) != PackageManager.PERMISSION_GRANTED) {
           ActivityCompat.requestPermissions(this, arrayOf(fineLocation, coarseLocation), locationPermissionRequestCode)
        }
    }

    /**
     * Sets listeners for buttons: scan and confirm
     *
     */
    private fun setListeners() {
        scan_code.setOnClickListener {
            val intent = Intent(this@MainActivity, ScannedBarcodeActivity::class.java)
            startActivityForResult(intent, apiKeyIsPresent)
        }
        confirm_code.setOnClickListener {
            onClickConfirm()
        }
    }

    /**
     * React on click confirm button. Authorizes the user. In case of successful authorization, it allows you to proceed further.
     *
     */
    private fun onClickConfirm() {
        val response = CurrentLoggedDeviceRequest().execute("https://fast-badlands-93869.herokuapp.com/api/mobile/me", api_key.text.toString()).get()
        var jsonObject = JSONObject()
        try {
            jsonObject = JSONObject(response)
        } catch (e: Exception) {
            CommonUtil().showToast(this, R.string.warning_when_key_is_not_valid)
        }

        if (api_key.text.toString() != "" && !jsonObject.toString().equals("{}") && jsonObject.get("key") == api_key.text.toString()) {
            isLogged = true
            saveData()

            val intent = Intent(this@MainActivity, MapsActivity::class.java)
            intent.putExtra("api_key", api_key.text.toString())
            startActivity(intent)
        } else {
            CommonUtil().showToast(this, R.string.warning_when_key_is_not_valid)
        }
    }

    /**
     * Loads information transferred when activity changes.
     *
     * @return false if you exit the application
     */
    private fun loadExtras(): Boolean {
        if (intent.getBooleanExtra("exit", false)) {
            finish()
            return true
        }
        if (intent.getStringExtra("is_logged") == "false") {
            isLogged = false
            saveData()
        }
        return false
    }

    /**
     * Saves shared preferences - userIsLogged.
     *
     */
    private fun saveData() {
        val sharedPreferences = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(userIsLogged, isLogged)
        editor.apply()
    }

    /**
     * Loads shared preferences.
     *
     */
    private fun loadData() {
        val sharedPreferences = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)
        isLogged = sharedPreferences.getBoolean(userIsLogged, false)
    }

    /**
     * The method is run when the activity started returns the result.
     *
     * @param requestCode - request code
     * @param resultCode - result code
     * @param data - intent from which the result comes
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == apiKeyIsPresent) {
            if (resultCode == Activity.RESULT_OK) {
                api_key.editableText.clear()
                api_key.editableText.append(data?.getStringExtra("api_key"))
            }
        }
    }

    /**
     * Responds to the user's decision. Grants permission or exits the application
     *
     * @param requestCode - request code
     * @param permissions - permissions
     * @param grantResults - array of results
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode) {
            locationPermissionRequestCode -> {
                if (grantResults.isNotEmpty()) {
                    grantResults.forEach {
                        if (it != PackageManager.PERMISSION_GRANTED) {
                            CommonUtil().showToast(this, R.string.explanation_of_location_usage)
                            finish()
                        }
                    }
                }
            }
        }
    }
}
