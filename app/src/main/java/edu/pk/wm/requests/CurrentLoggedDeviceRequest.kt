package edu.pk.wm.requests

import android.os.AsyncTask
import android.util.Log
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

/**
 * Deals with asynchronous execution of the GET request, authorizes the user
 *
 */
class CurrentLoggedDeviceRequest : AsyncTask<String, Void, String>() {

    /**
     * Executes a background query - asynchronously
     *
     * @param params - [0] - url, [1] - token
     * @return response with user key
     */
    override fun doInBackground(vararg params: String?): String {
        val responseBody = StringBuilder()
        var connection: HttpURLConnection? = null

        try {
            val url = params[0]!!
            val token = params[1]!!
            connection = URL(url).openConnection() as HttpURLConnection
            connection.requestMethod = "GET"
            connection.readTimeout = 1500
            connection.connectTimeout = 1500
            connection.setRequestProperty("Authorization", "Bearer $token")
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
            connection.connect()

            val inputStream: InputStream = connection.inputStream
            val inputStreamReader = InputStreamReader(inputStream)
            var inputStreamData: Int = inputStreamReader.read()
            while (inputStreamData != -1) {
                val current = inputStreamData.toChar()
                inputStreamData = inputStreamReader.read()
                responseBody.append(current)
            }
        } catch (e: Exception) {
            Log.e("edu.pk.wm.AsyncGetRequest.doInBackground", e.toString())
        } finally {
            connection?.disconnect()
        }

        return responseBody.toString()
    }
}