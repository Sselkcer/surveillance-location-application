package edu.pk.wm.handlers

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import edu.pk.wm.R

/**
 * Channel for sending external notifications
 *
 */
class Channel : Application() {

    /**
     * The method run when creating channel
     *
     */
    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
    }

    /**
     * Create notification channel
     *
     */
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, resources.getString(R.string.scan_code), NotificationManager.IMPORTANCE_HIGH)
            channel.description = resources.getString(R.string.scan_code)

            val manager = getSystemService(NotificationManager::class.java)
            manager?.createNotificationChannel(channel)
        }
    }

    companion object {
        const val channelId = "channel_id"
    }
}